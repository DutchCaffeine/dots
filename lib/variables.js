import Context from 'fig/Context.js';
const variables = {
    get identity() {
        if (Context.attributes.username === "alexander" ||
            Context.attributes.username === "dutchcaffeine") {
            return 'dutchcaffeine';
        }
        else {
            return 'unknown';
        }
    }
};
export default variables;

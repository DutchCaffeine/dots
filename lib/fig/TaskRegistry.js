var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
};
var _callbacks;
export default class TaskRegistry {
    constructor() {
        _callbacks.set(this, void 0);
        __classPrivateFieldSet(this, _callbacks, new Map());
    }
    register(aspect, callback, name) {
        if (!__classPrivateFieldGet(this, _callbacks).has(aspect)) {
            __classPrivateFieldGet(this, _callbacks).set(aspect, []);
        }
        __classPrivateFieldGet(this, _callbacks).get(aspect).push([callback, name]);
    }
    get(aspect) {
        return __classPrivateFieldGet(this, _callbacks).get(aspect) || [];
    }
}
_callbacks = new WeakMap();

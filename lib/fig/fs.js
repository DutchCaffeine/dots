/**
 * @file
 *
 * Wrappers for Node "fs" module methods that convert `Path` string-like
 * objects to strings before calling the underlying method.
 */
import * as fs from 'fs';
export function createWriteStream(path, options) {
    return fs.createWriteStream(path.toString(), options);
}
export function existsSync(path) {
    return fs.existsSync(path.toString());
}
export function readdirSync(path, options) {
    return fs.readdirSync(path.toString(), options);
}
const promises = {
    // This functions has overloads, so we have to use `any` types.
    readdir(directory, options) {
        return fs.promises.readdir(directory.toString(), options);
    },
    // This functions has overloads, so we have to use `any` types.
    readFile(path, ...args) {
        return fs.promises.readFile(path.toString(), ...args);
    },
    stat(path) {
        return fs.promises.stat(path.toString());
    },
    utimes(path, atime, mtime) {
        return fs.promises.utimes(path.toString(), atime, mtime);
    },
    writeFile(path, data, options) {
        return fs.promises.writeFile(path.toString(), data, options);
    },
};
export { promises };

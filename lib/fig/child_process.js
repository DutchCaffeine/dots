/**
 * @file
 *
 * Wrappers for Node "child_process" module methods that convert `Path`
 * string-like objects to strings before calling the underlying method.
 */
import * as child_process from 'child_process';
// TODO: see if I can do this with a few less "any"; necessary because there are
// so many overloads of these functions.
let spawn;
spawn = ((command, ...args) => {
    return child_process.spawn(command.toString(), ...args);
});
export { spawn };
let spawnSync;
spawnSync = (command, ...args) => {
    return child_process.spawnSync(command.toString(), ...args);
};
export { spawnSync };

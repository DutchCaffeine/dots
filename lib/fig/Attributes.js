var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
};
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
};
var _gid, _groupNames, _home, _platform, _uid, _umask, _username;
import * as os from 'os';
import id from './posix/id.js';
import stringify from './stringify.js';
import assertMode from './fs/assertMode.js';
/**
 * Immutable system attributes (read-only).
 */
export default class Attributes {
    constructor() {
        _gid.set(this, void 0);
        _groupNames.set(this, void 0);
        _home.set(this, void 0);
        _platform.set(this, void 0);
        _uid.set(this, void 0);
        _umask.set(this, void 0);
        _username.set(this, void 0);
    }
    get gid() {
        if (typeof __classPrivateFieldGet(this, _gid) !== 'number') {
            __classPrivateFieldSet(this, _gid, process.getgid());
        }
        return __classPrivateFieldGet(this, _gid);
    }
    get group() {
        return this.groupNames[0];
    }
    get groupNames() {
        if (!__classPrivateFieldGet(this, _groupNames)) {
            __classPrivateFieldSet(this, _groupNames, id());
        }
        return __classPrivateFieldGet(this, _groupNames);
    }
    get home() {
        if (!__classPrivateFieldGet(this, _home)) {
            __classPrivateFieldSet(this, _home, os.homedir());
        }
        return __classPrivateFieldGet(this, _home);
    }
    get platform() {
        if (!__classPrivateFieldGet(this, _platform)) {
            const uname = os.type();
            if (uname === 'Darwin') {
                __classPrivateFieldSet(this, _platform, 'darwin');
            }
            else if (uname === 'Linux') {
                __classPrivateFieldSet(this, _platform, 'linux');
                if (os.release().toLowerCase().includes('microsoft')) {
                    __classPrivateFieldSet(this, _platform, 'linux_wsl');
                }
            }
            else {
                throw new Error(`Unsupported platform ${stringify(uname)}`);
            }
        }
        return __classPrivateFieldGet(this, _platform);
    }
    get uid() {
        if (typeof __classPrivateFieldGet(this, _uid) !== 'number') {
            __classPrivateFieldSet(this, _uid, typeof process.getuid === 'function' ? process.getuid() : -1);
        }
        return __classPrivateFieldGet(this, _uid);
    }
    get umask() {
        if (!__classPrivateFieldGet(this, _umask)) {
            // Can't just read this, have to set it and restore it.
            //
            // https://nodejs.org/api/process.html#process_process_umask_mask
            const umask = process.umask(0o022);
            process.umask(umask);
            const paddedUmask = umask.toString(8).padStart(4, '0');
            assertMode(paddedUmask);
            __classPrivateFieldSet(this, _umask, paddedUmask);
        }
        return __classPrivateFieldGet(this, _umask);
    }
    get username() {
        if (!__classPrivateFieldGet(this, _username)) {
            __classPrivateFieldSet(this, _username, os.userInfo().username);
        }
        return __classPrivateFieldGet(this, _username);
    }
}
_gid = new WeakMap(), _groupNames = new WeakMap(), _home = new WeakMap(), _platform = new WeakMap(), _uid = new WeakMap(), _umask = new WeakMap(), _username = new WeakMap();

import Context from '../Context.js';
export default {
    get check() {
        return Context.currentOptions.check;
    },
    get focused() {
        return Context.currentOptions.focused;
    },
    get logLevel() {
        return Context.currentOptions.logLevel;
    },
    get step() {
        return Context.currentOptions.step;
    },
    get testsOnly() {
        return Context.currentOptions.testsOnly;
    },
};

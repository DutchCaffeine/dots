import ErrorWithMetadata from '../ErrorWithMetadata.js';
export default function fail(reason) {
    throw new ErrorWithMetadata(reason);
}

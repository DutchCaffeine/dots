import { join } from 'path';
import Context from '../Context.js';
import { readdirSync } from '../fs.js';
import globToRegExp from '../globToRegExp.js';
import path from '../path.js';
// TODO: think about exporting these separately (from separate files)
/**
 * Given `components` returns an aspect-local path corresponding to the
 * currently active aspect; eg:
 *
 *      aspects/${aspect}/files/${name[0]}/${name[1]}...
 */
export function file(...components) {
    return resource('files', ...components);
}
export function files(glob) {
    return resources('files', glob);
}
export function support(...components) {
    return resource('support', ...components);
}
export function template(...components) {
    return resource('templates', ...components);
}
export function templates(glob) {
    return resources('templates', glob);
}
function resource(subdirectory, ...components) {
    return path('aspects', Context.currentAspect, subdirectory, ...components);
}
/**
 * Very simple glob-based file search (only supports simple patterns like
 * "*.foo" and "thing/*.bar").
 */
function resources(subdirectory, glob) {
    function traverse(current, components) {
        return readdirSync(current, { withFileTypes: true })
            .filter((entry) => entry.isDirectory() || entry.isFile())
            .filter(({ name }) => components[0].test(name))
            .flatMap((entry) => {
            const next = path(current).join(entry.name);
            if (entry.isDirectory() && components.length > 1) {
                return traverse(next, components.slice(1));
            }
            else {
                return next;
            }
        });
    }
    return traverse(join('aspects', Context.currentAspect, subdirectory), path(glob).components.map((component) => globToRegExp(component)));
}

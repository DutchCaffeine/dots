import Context from '../Context.js';
import assert from '../assert.js';
import path from '../path.js';
export default function variable(name, fallback) {
    const variables = Context.currentVariables;
    return variables.hasOwnProperty(name) ? variables[name] : fallback || null;
}
variable.array = (name, fallback) => {
    const value = variable(name, fallback);
    assert.JSONArray(value, `Expected variable ${name} to be an array`);
    return value;
};
variable.object = (name, fallback) => {
    const value = variable(name, fallback);
    assert.JSONObject(value, `Expected variable ${name} to be an object`);
    return value;
};
variable.path = (name, fallback) => {
    const value = variable.string(name, fallback);
    return path(value);
};
variable.paths = (name, fallback) => {
    const value = variable.array(name, fallback);
    return value.map((v) => {
        assert(typeof v === 'string', `Expected variable ${name} to be an array of strings but it contained a ${typeof v}`);
        return path(v);
    });
};
variable.string = (name, fallback) => {
    const value = variable(name, fallback);
    assert(typeof value === 'string', `Expected variable ${name} to have type string but it was ${typeof value}`);
    return value;
};

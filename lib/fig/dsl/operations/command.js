import Context from '../../Context.js';
import ErrorWithMetadata from '../../ErrorWithMetadata.js';
import { debug, log } from '../../console.js';
import stat from '../../fs/stat.js';
import path from '../../path.js';
import run from '../../run.js';
import stringify from '../../stringify.js';
/**
 * Implements basic shell expansion (of ~).
 */
export default async function command(executable, args, options = {}) {
    var _a, _b, _c;
    const description = [executable, ...args].join(' ');
    if (options.creates) {
        const stats = await stat(path(options.creates).expand);
        if (stats instanceof Error) {
            throw stats;
        }
        if (stats !== null) {
            Context.informSkipped(`command \`${description}\``);
            return null;
        }
    }
    try {
        log.debug(`Run command \`${description}\` with options: ${stringify(options)}`);
        if ((_a = Context.currentOptions) === null || _a === void 0 ? void 0 : _a.check) {
            Context.informSkipped(`command \`${description}\``);
            return null;
        }
        else {
            const result = await run(path(executable).expand, args.map((arg) => path(arg).expand), {
                chdir: options.chdir
                    ? path(options.chdir).expand
                    : undefined,
                env: options.env,
                passphrase: options.sudo
                    ? await Context.sudoPassphrase
                    : undefined,
            });
            if (options.failedWhen
                ? options.failedWhen(result)
                : result.status !== 0) {
                throw new ErrorWithMetadata(`command \`${description}\` failed`, {
                    ...result,
                    error: (_c = (_b = result.error) === null || _b === void 0 ? void 0 : _b.toString()) !== null && _c !== void 0 ? _c : null,
                });
            }
            Context.informChanged(`command \`${description}\``, options.notify);
            return result;
        }
    }
    catch (error) {
        if (error instanceof ErrorWithMetadata) {
            Context.informFailed(error);
        }
        else {
            debug(() => console.log(error));
            Context.informFailed(`command \`${description}\` failed`);
        }
    }
    return null;
}

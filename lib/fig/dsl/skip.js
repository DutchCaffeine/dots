import Context from '../Context.js';
export default function skip(name = Context.currentTask) {
    Context.informSkipped(name);
}

import { log } from './console.js';
// TODO: ansible also has: unreachable, rescued, ignored
// decide whether we need any of those.
export function changed(message) {
    log.notice(`Changed: ${message}`);
}
export function failed(message) {
    log.error(`Failed: ${message}`);
}
export function ok(message) {
    log.info(`Ok: ${message}`);
}
export function skipped(message) {
    log.info(`Skipped: ${message}`);
}

/**
 * vim: set nomodifiable :
 *
 * @generated
 */
import assert from '../assert.js';
import { assertJSONValue } from './JSONValue.js';
const AspectValues = [
    'dotfiles',
    'meta',
    'node',
];
const ASPECT = new Set(AspectValues);
export function assertAspect(value) {
    assert(ASPECT.has(value));
}
export function assertProject(json) {
    assert(typeof json === 'object' && json);
    const missingKeys = ['platforms'].filter(key => {
        return !json.hasOwnProperty(key);
    });
    assert(!missingKeys.length);
    const allowedKeys = new Set(['platforms', 'profiles', 'variables']);
    const excessKeys = Object.keys(json).filter(key => {
        return !allowedKeys.has(key);
    });
    assert(!excessKeys.length);
    if (json.hasOwnProperty('platforms')) {
        const platforms = json.platforms;
        assert(typeof platforms === 'object' && platforms);
        const missingKeys = ['darwin', 'linux', 'linux_wsl'].filter(key => {
            return !platforms.hasOwnProperty(key);
        });
        assert(!missingKeys.length);
        const allowedKeys = new Set(['darwin', 'linux', 'linux_wsl']);
        const excessKeys = Object.keys(platforms).filter(key => {
            return !allowedKeys.has(key);
        });
        assert(!excessKeys.length);
        if (platforms.hasOwnProperty('darwin')) {
            const darwin = platforms.darwin;
            assert(typeof darwin === 'object' && darwin);
            const missingKeys = ['aspects'].filter(key => {
                return !darwin.hasOwnProperty(key);
            });
            assert(!missingKeys.length);
            const allowedKeys = new Set(['aspects', 'variables']);
            const excessKeys = Object.keys(darwin).filter(key => {
                return !allowedKeys.has(key);
            });
            assert(!excessKeys.length);
            if (darwin.hasOwnProperty('aspects')) {
                const aspects = darwin.aspects;
                assert(Array.isArray(aspects));
                aspects.forEach(assertAspect);
            }
            if (darwin.hasOwnProperty('variables')) {
                const variables = darwin.variables;
                assert(typeof variables === 'object' && variables);
                Object.values(variables).forEach(assertJSONValue);
            }
        }
        if (platforms.hasOwnProperty('linux')) {
            const linux = platforms.linux;
            assert(typeof linux === 'object' && linux);
            const missingKeys = ['aspects'].filter(key => {
                return !linux.hasOwnProperty(key);
            });
            assert(!missingKeys.length);
            const allowedKeys = new Set(['aspects', 'variables']);
            const excessKeys = Object.keys(linux).filter(key => {
                return !allowedKeys.has(key);
            });
            assert(!excessKeys.length);
            if (linux.hasOwnProperty('aspects')) {
                const aspects = linux.aspects;
                assert(Array.isArray(aspects));
                aspects.forEach(assertAspect);
            }
            if (linux.hasOwnProperty('variables')) {
                const variables = linux.variables;
                assert(typeof variables === 'object' && variables);
                Object.values(variables).forEach(assertJSONValue);
            }
        }
        if (platforms.hasOwnProperty('linux_wsl')) {
            const linux_wsl = platforms.linux_wsl;
            assert(typeof linux_wsl === 'object' && linux_wsl);
            const missingKeys = ['aspects'].filter(key => {
                return !linux_wsl.hasOwnProperty(key);
            });
            assert(!missingKeys.length);
            const allowedKeys = new Set(['aspects', 'variables']);
            const excessKeys = Object.keys(linux_wsl).filter(key => {
                return !allowedKeys.has(key);
            });
            assert(!excessKeys.length);
            if (linux_wsl.hasOwnProperty('aspects')) {
                const aspects = linux_wsl.aspects;
                assert(Array.isArray(aspects));
                aspects.forEach(assertAspect);
            }
            if (linux_wsl.hasOwnProperty('variables')) {
                const variables = linux_wsl.variables;
                assert(typeof variables === 'object' && variables);
                Object.values(variables).forEach(assertJSONValue);
            }
        }
    }
    if (json.hasOwnProperty('profiles')) {
        const profiles = json.profiles;
        assert(typeof profiles === 'object' && profiles);
        const valid = Object.values(profiles).every((value) => {
            assert(typeof value === 'object' && value);
            const missingKeys = ['pattern'].filter(key => {
                return !value.hasOwnProperty(key);
            });
            assert(!missingKeys.length);
            const allowedKeys = new Set(['pattern', 'variables']);
            const excessKeys = Object.keys(value).filter(key => {
                return !allowedKeys.has(key);
            });
            assert(!excessKeys.length);
            if (value.hasOwnProperty('pattern')) {
                const pattern = value.pattern;
                assert(typeof pattern === 'string');
            }
            if (value.hasOwnProperty('variables')) {
                const variables = value.variables;
                assert(typeof variables === 'object' && variables);
                Object.values(variables).forEach(assertJSONValue);
            }
            return true;
        });
        assert(valid);
    }
    if (json.hasOwnProperty('variables')) {
        const variables = json.variables;
        assert(typeof variables === 'object' && variables);
        Object.values(variables).forEach(assertJSONValue);
    }
}

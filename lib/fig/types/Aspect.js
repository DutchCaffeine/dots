/**
 * vim: set nomodifiable :
 *
 * @generated
 */
import assert from '../assert.js';
import { assertJSONValue } from './JSONValue.js';
export function assertAspect(json) {
    assert(typeof json === 'object' && json);
    const missingKeys = ['description'].filter(key => {
        return !json.hasOwnProperty(key);
    });
    assert(!missingKeys.length);
    const allowedKeys = new Set(['description', 'variables']);
    const excessKeys = Object.keys(json).filter(key => {
        return !allowedKeys.has(key);
    });
    assert(!excessKeys.length);
    if (json.hasOwnProperty('description')) {
        const description = json.description;
        assert(typeof description === 'string');
    }
    if (json.hasOwnProperty('variables')) {
        const variables = json.variables;
        assert(typeof variables === 'object' && variables);
        Object.values(variables).forEach(assertJSONValue);
    }
}

var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
};
var _attributes, _compiler, _counts, _currentAspect, _currentOptions, _currentTask, _currentVariables, _handlers, _sudoPassphrase, _tasks, _variables;
import Attributes from './Attributes.js';
import ErrorWithMetadata from './ErrorWithMetadata.js';
import Compiler from './Compiler.js';
import HandlerRegistry from './HandlerRegistry.js';
import TaskRegistry from './TaskRegistry.js';
import VariableRegistry from './VariableRegistry.js';
import assert from './assert.js';
import prompt from './prompt.js';
import * as status from './status.js';
/**
 * Try to keep nasty global state all together in one place.
 *
 * Global state helps keep our "aspect" DSL as lightweight/implicit as
 * possible.
 */
class Context {
    constructor() {
        _attributes.set(this, void 0);
        _compiler.set(this, void 0);
        _counts.set(this, void 0);
        _currentAspect.set(this, void 0);
        _currentOptions.set(this, void 0);
        _currentTask.set(this, void 0);
        _currentVariables.set(this, void 0);
        _handlers.set(this, void 0);
        _sudoPassphrase.set(this, void 0);
        _tasks.set(this, void 0);
        // TODO: rename stuff to avoid confusion about `variables`
        // (VariableRegistry) vs `currentVariables` (merged variables set
        // from main.ts).
        _variables.set(this, void 0);
        __classPrivateFieldSet(this, _attributes, new Attributes());
        __classPrivateFieldSet(this, _compiler, new Compiler());
        __classPrivateFieldSet(this, _counts, {
            changed: 0,
            failed: 0,
            ok: 0,
            skipped: 0,
        });
        __classPrivateFieldSet(this, _handlers, new HandlerRegistry());
        __classPrivateFieldSet(this, _tasks, new TaskRegistry());
        __classPrivateFieldSet(this, _variables, new VariableRegistry());
    }
    compile(path) {
        return __classPrivateFieldGet(this, _compiler).compile(path);
    }
    informChanged(message, notify) {
        __classPrivateFieldGet(this, _counts).changed++;
        if (notify !== undefined) {
            assert(__classPrivateFieldGet(this, _currentAspect));
            __classPrivateFieldGet(this, _handlers).notify(__classPrivateFieldGet(this, _currentAspect), `${__classPrivateFieldGet(this, _currentAspect)} | ${notify}`);
        }
        status.changed(message);
    }
    informFailed(...args) {
        let error;
        if (typeof args[0] === 'string') {
            error = new ErrorWithMetadata(args[0], args[1]);
        }
        else {
            error = args[0];
        }
        __classPrivateFieldGet(this, _counts).failed++;
        status.failed(error.message);
        throw error;
    }
    informOk(message) {
        __classPrivateFieldGet(this, _counts).ok++;
        status.ok(message);
    }
    informSkipped(message) {
        __classPrivateFieldGet(this, _counts).skipped++;
        status.skipped(message);
    }
    async withContext({ aspect, options, task, variables, }, callback) {
        let previousAspect = __classPrivateFieldGet(this, _currentAspect);
        let previousOptions = __classPrivateFieldGet(this, _currentOptions);
        let previousTask = __classPrivateFieldGet(this, _currentTask);
        let previousVariables = __classPrivateFieldGet(this, _currentVariables);
        try {
            __classPrivateFieldSet(this, _currentAspect, aspect);
            __classPrivateFieldSet(this, _currentOptions, options);
            __classPrivateFieldSet(this, _currentTask, task);
            __classPrivateFieldSet(this, _currentVariables, variables);
            await callback();
        }
        finally {
            __classPrivateFieldSet(this, _currentAspect, previousAspect);
            __classPrivateFieldSet(this, _currentOptions, previousOptions);
            __classPrivateFieldSet(this, _currentTask, previousTask);
            __classPrivateFieldSet(this, _currentVariables, previousVariables);
        }
    }
    get attributes() {
        return __classPrivateFieldGet(this, _attributes);
    }
    get counts() {
        return __classPrivateFieldGet(this, _counts);
    }
    get currentAspect() {
        assert(__classPrivateFieldGet(this, _currentAspect));
        return __classPrivateFieldGet(this, _currentAspect);
    }
    set currentAspect(aspect) {
        __classPrivateFieldSet(this, _currentAspect, aspect);
    }
    get currentTask() {
        assert(__classPrivateFieldGet(this, _currentTask));
        return __classPrivateFieldGet(this, _currentTask);
    }
    set currentTask(task) {
        __classPrivateFieldSet(this, _currentTask, task);
    }
    get currentVariables() {
        assert(__classPrivateFieldGet(this, _currentVariables));
        return __classPrivateFieldGet(this, _currentVariables);
    }
    set currentVariables(variables) {
        __classPrivateFieldSet(this, _currentVariables, variables);
    }
    get currentOptions() {
        assert(__classPrivateFieldGet(this, _currentOptions));
        return __classPrivateFieldGet(this, _currentOptions);
    }
    get handlers() {
        return __classPrivateFieldGet(this, _handlers);
    }
    get sudoPassphrase() {
        if (!__classPrivateFieldGet(this, _sudoPassphrase)) {
            __classPrivateFieldSet(this, _sudoPassphrase, prompt(`Password [will not be echoed]: `, {
                private: true,
            }));
        }
        return __classPrivateFieldGet(this, _sudoPassphrase);
    }
    // TODO: note that I might be going overboard here with private
    // variables for stuff that I really don't have to worry about getting
    // meddled with
    get tasks() {
        return __classPrivateFieldGet(this, _tasks);
    }
    get variables() {
        return __classPrivateFieldGet(this, _variables);
    }
}
_attributes = new WeakMap(), _compiler = new WeakMap(), _counts = new WeakMap(), _currentAspect = new WeakMap(), _currentOptions = new WeakMap(), _currentTask = new WeakMap(), _currentVariables = new WeakMap(), _handlers = new WeakMap(), _sudoPassphrase = new WeakMap(), _tasks = new WeakMap(), _variables = new WeakMap();
export default new Context();

var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
};
var _callbacks, _notifications;
// TODO: Dry this up... handler, task, variable registries are all very similar
export default class HandlerRegistry {
    constructor() {
        _callbacks.set(this, void 0);
        _notifications.set(this, void 0);
        __classPrivateFieldSet(this, _callbacks, new Map());
        __classPrivateFieldSet(this, _notifications, new Map());
    }
    notify(aspect, name) {
        if (!__classPrivateFieldGet(this, _notifications).has(aspect)) {
            __classPrivateFieldGet(this, _notifications).set(aspect, new Set());
        }
        __classPrivateFieldGet(this, _notifications).get(aspect).add(name);
    }
    register(aspect, callback, name) {
        if (!__classPrivateFieldGet(this, _callbacks).has(aspect)) {
            __classPrivateFieldGet(this, _callbacks).set(aspect, []);
        }
        if (__classPrivateFieldGet(this, _callbacks).get(aspect).some(([, registeredName]) => {
            return name === registeredName;
        })) {
            throw new Error(`Handler has already be registered with name ${name} for aspect ${aspect}`);
        }
        __classPrivateFieldGet(this, _callbacks).get(aspect).push([callback, name]);
    }
    get(aspect) {
        return {
            callbacks: __classPrivateFieldGet(this, _callbacks).get(aspect) || [],
            notifications: __classPrivateFieldGet(this, _notifications).get(aspect) || new Set(),
        };
    }
}
_callbacks = new WeakMap(), _notifications = new WeakMap();

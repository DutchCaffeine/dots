export default class ErrorWithMetadata extends Error {
    constructor(message, metadata) {
        super(message);
        this.metadata = metadata;
    }
}

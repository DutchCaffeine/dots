var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
};
var _callbacks;
export default class VariableRegistry {
    constructor() {
        _callbacks.set(this, void 0);
        __classPrivateFieldSet(this, _callbacks, new Map());
    }
    register(aspect, callback) {
        if (__classPrivateFieldGet(this, _callbacks).has(aspect)) {
            throw new Error(`Variables have already been registered for aspect ${aspect}`);
        }
        __classPrivateFieldGet(this, _callbacks).set(aspect, callback);
    }
    get(aspect) {
        return __classPrivateFieldGet(this, _callbacks).get(aspect) || (() => ({}));
    }
}
_callbacks = new WeakMap();

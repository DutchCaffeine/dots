/**
 * https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
 * https://stackoverflow.com/a/41407246/2103996
 */
const BOLD = '\x1b[1m';
const GREEN = '\x1b[32m';
const PURPLE = '\x1b[35m';
const RED = '\x1b[31m';
const RESET = '\x1b[0m';
const REVERSE = '\x1b[7m';
const YELLOW = '\x1b[33m';
function bold(input, ...interpolations) {
    if (Array.isArray(input)) {
        return style(interpolate(input, interpolations), BOLD);
    }
    else {
        return style(input, BOLD);
    }
}
function green(input, ...interpolations) {
    if (Array.isArray(input)) {
        return style(interpolate(input, interpolations), GREEN);
    }
    else {
        return style(input, GREEN);
    }
}
function purple(input, ...interpolations) {
    if (Array.isArray(input)) {
        return style(interpolate(input, interpolations), PURPLE);
    }
    else {
        return style(input, PURPLE);
    }
}
function red(input, ...interpolations) {
    if (Array.isArray(input)) {
        return style(interpolate(input, interpolations), RED);
    }
    else {
        return style(input, RED);
    }
}
function reverse(input, ...interpolations) {
    if (Array.isArray(input)) {
        return style(interpolate(input, interpolations), REVERSE);
    }
    else {
        return style(input, REVERSE);
    }
}
function yellow(input, ...interpolations) {
    if (Array.isArray(input)) {
        return style(interpolate(input, interpolations), YELLOW);
    }
    else {
        return style(input, YELLOW);
    }
}
function style(text, escape) {
    return `${escape}${text}${RESET}`;
}
function interpolate(strings, interpolations) {
    return strings.reduce((acc, string, i) => {
        if (i < interpolations.length) {
            return acc + string + String(interpolations[i]);
        }
        else {
            return acc + string;
        }
    }, '');
}
const COLORS = {
    bold(strings, ...interpolations) {
        const result = bold(strings, ...interpolations);
        if (typeof this === 'function') {
            return this.call(null, result);
        }
        else {
            return result;
        }
    },
    green(strings, ...interpolations) {
        const result = green(strings, ...interpolations);
        if (typeof this === 'function') {
            return this.call(null, result);
        }
        else {
            return result;
        }
    },
    purple(strings, ...interpolations) {
        const result = purple(strings, ...interpolations);
        if (typeof this === 'function') {
            return this.call(null, result);
        }
        else {
            return result;
        }
    },
    red(strings, ...interpolations) {
        const result = red(strings, ...interpolations);
        if (typeof this === 'function') {
            return this.call(null, result);
        }
        else {
            return result;
        }
    },
    reverse(strings, ...interpolations) {
        const result = reverse(strings, ...interpolations);
        if (typeof this === 'function') {
            return this.call(null, result);
        }
        else {
            return result;
        }
    },
    yellow(strings, ...interpolations) {
        const result = yellow(strings, ...interpolations);
        if (typeof this === 'function') {
            return this.call(null, result);
        }
        else {
            return result;
        }
    },
};
export default {
    bold: Object.assign(COLORS.bold, COLORS),
    green: Object.assign(COLORS.green, COLORS),
    purple: Object.assign(COLORS.purple, COLORS),
    red: Object.assign(COLORS.red, COLORS),
    reverse: Object.assign(COLORS.reverse, COLORS),
    yellow: Object.assign(COLORS.yellow, COLORS),
};

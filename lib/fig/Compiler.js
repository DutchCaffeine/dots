var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
};
var _compiled;
// import {log} from './console.js';
import { promises as fs } from './fs.js';
import { compile, fill } from './template.js';
/**
 * Template compiler that manages a cache of compiled templates.
 */
export default class Compiler {
    constructor() {
        _compiled.set(this, void 0);
        __classPrivateFieldSet(this, _compiled, new Map());
    }
    async compile(path) {
        // Convert potential Path string-like back to primitive.
        path = path.toString();
        const map = __classPrivateFieldGet(this, _compiled);
        if (!map.has(path)) {
            const source = await fs.readFile(path, 'utf8');
            const compiled = compile(source);
            // BUG: too verbose?
            // log.debug(`Compiled template source:\n\n${compiled}\n`);
            map.set(path, {
                fill(scope) {
                    return fill(compiled, scope);
                },
            });
        }
        return map.get(path);
    }
}
_compiled = new WeakMap();
